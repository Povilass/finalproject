import sys
import json
from finalproject.models import Coin
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Import coin data from a scraper'

    def handle(self, *args, **options):
        for line in sys.stdin:
            data = json.loads(line)
            Coin.objects.create(name=data['coin_name'])
