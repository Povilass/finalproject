from django.contrib import admin
from django.urls import path
from finalproject import views


urlpatterns = [
    path("", views.home, name="home"),
    path("register", views.register_request, name="register"),
    path("login", views.login_request, name="login"),
    path("logout", views.logout_request, name= "logout"),
    path('admin/', admin.site.urls),
    path('', views.home, name="home"),
    path('prices/', views.prices, name="prices"),
    path('subscribe/<coin_id>', views.subscribe, name='subscribe'),
]
