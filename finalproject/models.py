from django.contrib.auth.models import User
from django.db import models
from django.db.models import UniqueConstraint


class Coin(models.Model):
    name = models.CharField(max_length=100)


class Subscription(models.Model):
    coin = models.ForeignKey(Coin, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            UniqueConstraint(fields=['coin', 'user'], name='unique_subscription'),
        ]
