from io import StringIO
import json

import pytest
from django.core.management import call_command
from django.contrib.auth.models import User

from finalproject.models import Coin, Subscription


@pytest.mark.django_db
def test_import_coin_data(mocker):
    data = {
        "coin_name": "Frz Solar System",
        "coin_name_short": "FRZSS",
        "coin_price": "0.0000005484",
    }
    mocker.patch("sys.stdin", StringIO(json.dumps(data)))
    stdout = StringIO()
    call_command("import_coin_data", stdout=stdout)
    assert Coin.objects.filter(name="Frz Solar System").exists()





@pytest.mark.django_db
def test_subscribe_to_coin(client):
    user = User.objects.create_user(username='user', password='secret')
    client.login(username='user', password='secret')
    coin = Coin.objects.create(name='Bitcoin')
    res = client.get(f'/subscribe/{coin.id}')
    assert res.status_code == 302
    assert res.headers['location'] == '/'
    assert Subscription.objects.filter(coin=coin, user=user).exists()


@pytest.mark.django_db
def test_subscribe_to_coin_existing(client):
    user = User.objects.create_user(username='user', password='secret')
    client.login(username='user', password='secret')
    coin = Coin.objects.create(name='Bitcoin')
    Subscription.objects.create(coin=coin, user=user)

    res = client.get(f'/subscribe/{coin.id}')
    assert res.status_code == 409
    assert Subscription.objects.filter(coin=coin, user=user).count() == 1
